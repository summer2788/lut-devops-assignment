FROM node:12

WORKDIR /app  
# tell docker where the working dir is 

COPY package*.json ./

RUN npm install 
# lightweight install version compared to npm install 

COPY . . 
# first dot(.) : current local file second dot(.) : container 

EXPOSE 3000

CMD ["npm", "start"]


